# ddev-typo3
> testing out ddev with typo3 🙄

## Prerequisites 📇
1. Install: \
   docker ce                     https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce \
   curl                         `sudo apt install curl` \
   docker compose                https://docs.docker.com/compose/install/#prerequisites \
   ddev                          https://ddev.readthedocs.io/en/latest/ 

2. Follow manual:\
   ddev typo3                    https://docs.typo3.org/typo3cms/ContributionWorkflowGuide/Appendix/SettingUpTypo3Ddev.html#settting-up-typo3-with-ddev

3. Install: \
   composer                      `sudo apt install composer` \
   php7 extensions               https://stackoverflow.com/questions/50242053/after-ubuntu-18-04-upgrade-php7-2-curl-cannot-be-installed \
   yarn                          https://yarnpkg.com/en/docs/install#debian-stable 

4. Follow manual: \
   ddev typo3                    https://docs.typo3.org/typo3cms/ContributionWorkflowGuide/Appendix/SettingUpTypo3Ddev.html#settting-up-typo3-with-ddev 

## Get started 🚀
```bash
git clone https://gitlab.com/SimonSiefke/ddev-typo3.git &&
cd ddev-typo3 &&
composer install &&
ddev config &&
cd Build && 
yarn &&
cd .. &&
ddev start && 
firefox ddev-typo3.ddev.local/typo3
```

## Common Errors 😫
1. **Docker can't mount /Library/...** -> include Library in Docker mount paths 
1. **composer: Your requirements could not be resolved to an installable set of packages.** -> make sure you have installed the php extensions (https://stackoverflow.com/questions/50242053/after-ubuntu-18-04-upgrade-php7-2-curl-cannot-be-installed)
1. **Failed to start ddev-typo3: Unable to listen on required ports, localhost port 80 is in use** -> make sure your apache-server or your nginx-server is turned off (`apache2ctl stop` or `sudo service nginx stop`) 
1. **Nginx 403** -> make sure that there was no error when you ran `composer install` (see 2) and try again ```composer install```